# Oracle (with sqlplus)

You want to use Oracle with sqlplus ? This is the right place !

You can use this image to connect to an Oracle database with sqlplus and interact with persistent databases !

## How to use :

- Clone repository
- Login to registry : `docker login container-registry.oracle.com`
- Create a folder named `data` at the root of the repository and give it the right permissions (`chmod -R 777 ./data`)
- Run `make up` to start the container (first time will take a while to download the image + configure oracle)
- You can now connect to the database with sqlplus using `make db` (default user is `myuser` with password `mypassword`)

### How to connect as sysdba :

- Run `make dba` to connect as sysdba.

## Additionnal informations :

- You can type `make help` to see all availables commands.

- If you have an error while oracle is initializing, make sure data folder have the right permissions (`chmod -R 777 ./data` on repository's root path).
