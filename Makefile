DOCKER_CONTAINER_NAME = oracle
DC = docker compose -f docker-compose.yml

.PHONY: up down db dba
.DEFAULT_GOAL := help

up: ## Start the docker container
	$(DC) up --build -d

do: ## Stop the docker container
	$(DC) down

db: ## Connect to the database as simple user
	docker exec -it $(DOCKER_CONTAINER_NAME) sqlplus myuser/mypassword@//localhost:1521/ORCLCDB

dba: ## Connect to the database as sysdba
	docker exec -it $(DOCKER_CONTAINER_NAME) sqlplus sys/database@//localhost:1521/ORCLCDB as sysdba

ex: ## Enter the container
	docker exec -it $(DOCKER_CONTAINER_NAME) /bin/bash

exa: ## Enter the container as root
	docker exec -it -u root $(DOCKER_CONTAINER_NAME) /bin/bash

import: prep-args ## Apply SQL file to the database as myuser
	@if [ -z "$(runargs)" ]; then \
		echo "Usage: make apply_sql SQL_FILE=<path_to_sql_file>"; \
	else \
		docker cp $(runargs) $(DOCKER_CONTAINER_NAME):/home/; \
		echo exit | docker exec -i $(DOCKER_CONTAINER_NAME) sqlplus -S myuser/mypassword@//localhost:1521/ORCLCDB @/home/$(notdir $(runargs)); \
		docker exec -it --user=root $(DOCKER_CONTAINER_NAME) rm /home/$(notdir $(runargs)); \
		echo "SQL file $(runargs) applied to the database"; \
	fi

iimport: prep-args ## Import SQL file to the database as myuser and stay in the container
	@if [ -z "$(runargs)" ]; then \
		echo "Usage: make interactive_apply_sql SQL_FILE=<path_to_sql_file>"; \
	else \
		docker cp $(runargs) $(DOCKER_CONTAINER_NAME):/home/; \
		docker exec -i $(DOCKER_CONTAINER_NAME) sqlplus -S myuser/mypassword@//localhost:1521/ORCLCDB @/home/$(notdir $(runargs)); \
	fi

help: ## Display this help
	@echo "Usage: make [target]"
	@echo "Targets:"
	@awk -F ':.*?## ' '/^[a-zA-Z0-9_-]+:.*?##/ { printf "  %-15s: %s\n", $$1, substr($$0, index($$0, $$2)) }' $(MAKEFILE_LIST)


prep-args: ## [Internal] Pre-recipe for commands with args
	@echo -e "\e[1;44m Please be aware that you cannot concatenate recipes with this command! \e[0m"
	$(eval runargs=$(wordlist 2, $(words $(MAKECMDGOALS)), $(MAKECMDGOALS)))


prep-find: prep-args ## [Internal] Pre-recipe for find commands
	@$(eval F_FIND_BUILT=$(patsubst %,-name '*%*' -o, $(wordlist 2,$(words $(runargs)),x $(runargs))) $(patsubst %,-name '*%*', $(lastword $(runargs))))