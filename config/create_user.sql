DECLARE
   user_count NUMBER;
BEGIN
   -- Check if the user already exists
   SELECT COUNT(*) INTO user_count FROM DBA_USERS WHERE USERNAME = 'MYUSER';

   IF user_count = 0 THEN
      -- If the user doesn't exist, create and grant privileges
      EXECUTE IMMEDIATE 'ALTER SESSION SET "_ORACLE_SCRIPT"=true';
      EXECUTE IMMEDIATE 'CREATE USER myuser IDENTIFIED BY mypassword';
   ELSE
      -- If the user already exists, you may choose to handle this case as needed
      DBMS_OUTPUT.PUT_LINE('User MYUSER already exists.');
   END IF;

   -- Grant privileges
   EXECUTE IMMEDIATE 'GRANT CREATE SESSION TO MYUSER';
   EXECUTE IMMEDIATE 'GRANT CREATE TABLE TO MYUSER';
   EXECUTE IMMEDIATE 'GRANT CREATE SEQUENCE TO MYUSER';
   EXECUTE IMMEDIATE 'GRANT CREATE VIEW TO MYUSER';
   EXECUTE IMMEDIATE 'GRANT CONNECT, RESOURCE, DBA TO MYUSER';
   EXECUTE IMMEDIATE 'GRANT UNLIMITED TABLESPACE TO MYUSER';

END;
/
